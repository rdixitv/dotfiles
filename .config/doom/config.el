(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/")
             t)
(add-to-list 'package-archives
             '("org" . "https://orgmode.org/elpa/")
             t)

(setq user-full-name "Raghav Dixit"
      user-mail-address "raghavd@disroot.org")

(setq eshell-rc-script "~/.config/doom/eshell/profile"
      eshell-visual-commands '("bash" "fish" "htop" "ssh" "top" "zsh"))

(setq doom-font (font-spec :family "JetBrainsMono NF" :size 24)
      doom-variable-pitch-font (font-spec :family "Noto Sans" :size 15)
      doom-big-font (font-spec :family "JetBrainsMono NF" :size 28))
(require 'auto-complete)
(global-auto-complete-mode t)

(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t)
    (doom-themes-visual-bell-config))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))
(setq global-prettify-symbols-mode t)

(load-theme 'doom-tokyo-night t)

(global-visual-line-mode t) ; enables word wrap
(setq kill-whole-line t)

(use-package doom-modeline
  :after eshell     ;; Make sure it gets hooked after eshell
  :hook (after-init . doom-modeline-init)
  :custom-face
  (mode-line ((t (:height 0.85))))
  (mode-line-inactive ((t (:height 0.85))))
  :custom
  (doom-modeline-height 15)
  (doom-modeline-bar-width 6)
  (doom-modeline-lsp t)
  (doom-modeline-github nil)
  (doom-modeline-mu4e nil)
  (doom-modeline-irc t)
  (doom-modeline-minor-modes t)
  (doom-modeline-persp-name nil)
  (doom-modeline-buffer-file-name-style 'truncate-except-project)
  (doom-modeline-major-mode-icon nil))

(defun rd/year-calendar (&optional year)
  (interactive)
  (require 'calendar)
  (let* (
      (current-year (number-to-string (nth 5 (decode-time (current-time)))))
      (month 0)
      (year (if year year (string-to-number (format-time-string "%Y" (current-time))))))
    (switch-to-buffer (get-buffer-create calendar-buffer))
    (when (not (eq major-mode 'calendar-mode))
      (calendar-mode))
    (setq displayed-month month)
    (setq displayed-year year)
    (setq buffer-read-only nil)
    (erase-buffer)
    ;; horizontal rows
    (dotimes (j 4)
      ;; vertical columns
      (dotimes (i 3)
        (calendar-generate-month
          (setq month (+ month 1))
          year
          ;; indentation / spacing between months
          (+ 5 (* 25 i))))
      (goto-char (point-max))
      (insert (make-string (- 10 (count-lines (point-min) (point-max))) ?\n))
      (widen)
      (goto-char (point-max))
      (narrow-to-region (point-max) (point-max)))
    (widen)
    (goto-char (point-min))
    (setq buffer-read-only t)))

(defun rd/scroll-year-calendar-forward (&optional arg event)
  "Scroll the yearly calendar by year in a forward direction."
  (interactive (list (prefix-numeric-value current-prefix-arg)
                     last-nonmenu-event))
  (unless arg (setq arg 0))
  (save-selected-window
    (if (setq event (event-start event)) (select-window (posn-window event)))
    (unless (zerop arg)
      (let* (
              (year (+ displayed-year arg)))
        (rd/year-calendar year)))
    (goto-char (point-min))
    (run-hooks 'calendar-move-hook)))

(defun rd/scroll-year-calendar-backward (&optional arg event)
  "Scroll the yearly calendar by year in a backward direction."
  (interactive (list (prefix-numeric-value current-prefix-arg)
                     last-nonmenu-event))
  (rd/scroll-year-calendar-forward (- (or arg 1)) event))

(map! :leader
      :desc "Scroll year calendar backward" "<left>" #'rd/scroll-year-calendar-backward
      :desc "Scroll year calendar forward" "<right>" #'rd/scroll-year-calendar-forward)

(defalias 'year-calendar 'rd/year-calendar)
(use-package calfw)
(use-package calfw-org)

(map! :leader
      (:prefix ("c h" . "Help info from clippy")
      :desc "Describe function under pointer" "f" #'clippy-describe-function
      :desc "Describe variable under pointer" "v" #'clippy-describe-variable))

(setq minimap-window-location 'right)
(map! :leader
      (:prefix ("t" . "toggle")
       :desc "Toggle minimap" "m" #'minimap-mode))

;; Get hybrid mode working (Emacs keybindings replace evil insert mode)
;(use-package evil
;  :custom
;  evil-disable-insert-state-bindings t)
;; (defalias 'evil-insert-state 'evil-emacs-state)
;; (define-key evil-emacs-state-map (kbd "<escape>") 'evil-normal-state)


(map! :leader
      :desc "Edit doom config"
      "- d c" #'(lambda () (interactive) (find-file "~/.config/doom/config.org"))
      :leader
      :desc "Edit doom packages"
      "- d p" #'(lambda () (interactive) (find-file "~/.config/doom/packages.el"))
      :leader
      :desc "Edit doom init"
      "- d i" #'(lambda () (interactive) (find-file "~/.config/doom/init.el"))
      :leader
      :desc "Edit eshell aliases"
      "- d a" #'(lambda () (interactive) (find-file "~/.config/doom/eshell/aliases"))
      :leader
      :desc "Edit fish config"
      "- f" #'(lambda () (interactive) (find-file "~/.config/fish/config.fish"))
      :leader
      :desc "Edit vifm config"
      "- v c" #'(lambda () (interactive) (find-file "~/.config/vifm/vifmrc"))
      :leader
      :desc "Rust Mode"
      "- r" #'rust-mode
      :leader
      :desc "Autocomplete mode"
      "- a" #'auto-complete-mode
      :leader
      :desc "Company Mode"
      "- c" #'company-mode
      :leader
      :desc "Toggle Neotree"
      "o n" #'neotree-toggle
      :leader
      :desc "Open Emacs Keybindings Cheat Sheet"
      "- e" #'(lambda () (interactive) (find-file "~/org-mode/emacs-vs-evil.org"))
      :leader
      :desc "Open mu4e"
      "o m" #'mu4e
      :leader
      :desc "Open ERC with Liberachat"
      "- l" #'(lambda ()
                (interactive)
                (erc-tls :server "irc.libera.chat"
                         :port "6667")))

(after! org
  (defun efs/org-font-setup ()
	  ;; Replace list hyphen with dot
	  (font-lock-add-keywords 'org-mode
				  '(("^ *\\([-]\\) "
				     (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

	  ;; Set faces for heading levels
	  (dolist (face '((org-level-1 . 2.0)
			  (org-level-2 . 1.8)
			  (org-level-3 . 1.4)
			  (org-level-4 . 1.1)
			  (org-level-5 . 1.1)
			  (org-level-6 . 1.1)
			  (org-level-7 . 1.1)
			  (org-level-8 . 1.1)))
	    (set-face-attribute (car face) nil :font "Noto Sans" :weight 'regular :height (cdr face)))

	  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
	  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
	  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
	  (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
	  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
	  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
	  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
	  (set-face-attribute 'org-checkbox nil :inherit 'varible-pitch))


    (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))


	(use-package org

	  ;; :hook (org-mode . efs/org-mode-setup)
	  :config
	  (setq org-directory "~/org-mode/"
		org-agenda-files '("~/org-mode")
		org-default-notes-file (expand-file-name "notes.org" org-directory)
		org-ellipsis " ▼ "
		org-log-done 'time
		;; org-journal-dir "~/Org/journal/"
		org-hide-emphasis-markers t
		;; ex. of org-link-abbrev-alist in action
		;; [[arch-wiki:Name_of_Page][Description]]
		org-todo-keywords        ; This overwrites the default org-todo-keywords
		  '((sequence
		     "TODO(t)"
		     "HOMEWORK(h)"
		     "CLASS(C)"
		     "PROJ(p)"
		     "TEST(T)"
		     "EVENT(e)"
		     "EXAM(E)"
		     "|"
		     "DONE(d)"
		     "CANCELLED(c)"
		     "ANYTIME(a)")))
	  ;(setq org-ellipsis " ▾")
	  (efs/org-font-setup))
 (defun dw/org-mode-visual-fill ()
  (setq visual-fill-column-width 110
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

;; (use-package visual-fill-column
;;   :defer t
  ;; :hook (org-mode . dw/org-mode-visual-fill))


	(use-package org-bullets
	  :after org
	  :hook (org-mode . org-bullets-mode)
	  :custom
	  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))
	(use-package toc-org)
    (dolist (mode '(org-mode-hook))

		  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(defun nolinum ()
  (interactive)
  (global-linum-mode 0))
 (add-hook 'org-mode-hook 'nolinum) ; Turn off line numbers in org mode
 (add-hook 'org-mode-hook 'visual-fill-column-mode)

(use-package org-tree-slide
  :custom
  (org-image-actual-width nil))

(define-key org-mode-map (kbd "<f8>") 'org-tree-slide-mode)
(define-key org-mode-map (kbd "S-<f8>") 'org-tree-slide-skip-done-toggle)

(setq org-support-shift-select t)
(setq org-clock-sound "~/Downloads/99595927.mp3")

;; (setq centaur-tabs-set-bar 'over
;;       centaur-tabs-set-icons t
;;       centaur-tabs-gray-out-icons 'buffer
;;       centaur-tabs-height 24
;;       centaur-tabs-set-modified-marker t
;;       centaur-tabs-style "bar"
;;       centaur-tabs-modified-marker "•")
;; (map! :leader
;;       :desc "Toggle tabs globally" "t c" #'centaur-tabs-mode
;;       :desc "Toggle tabs local display" "t C" #'centaur-tabs-local-mode)
;; (evil-define-key 'normal centaur-tabs-mode-map (kbd "g <right>") 'centaur-tabs-forward        ; default Doom binding is 'g t'
;;                                                (kbd "g <left>")  'centaur-tabs-backward       ; default Doom binding is 'g T'
;;                                                (kbd "g <down>")  'centaur-tabs-forward-group
;;
;                                                (kbd "g <up>")    'centaur-tabs-backward-group)

(setq confirm-kill-emacs nil)
(setq auto-save-default t
      make-backup-files t)

;; (load (expand-file-name "~/.quicklisp/slime-helper.el"))
;; (setq inferior-lisp-program "sbcl")

  ;; (use-package dired-rainbow
  ;;   :defer 2
  ;;   :config
  ;;   (dired-rainbow-define-chmod directory "#6cb2eb" "d.*")
  ;;   ;; (dired-rainbow-define html "#eb5286" ("css" "less" "sass" "scss" "htm" "html" "jhtm" "mht" "eml" "mustache" "xhtml"))
  ;;   ;; (dired-rainbow-define xml "#f2d024" ("xml" "xsd" "xsl" "xslt" "wsdl" "bib" "json" "msg" "pgn" "rss" "yaml" "yml" "rdata"))
  ;;   (dired-rainbow-define document "#9561e2" ("docm" "doc" "docx" "odb" "odt" "pdb" "pdf" "ps" "rtf" "djvu" "epub" "odp" "ppt" "pptx"))
  ;;   (dired-rainbow-define markdown "#ffed4a" ("org" "etx" "info" "markdown" "md" "mkd" "nfo" "pod" "rst" "tex" "textfile" "txt"))
  ;;   (dired-rainbow-define database "#6574cd" ("xlsx" "xls" "csv" "accdb" "db" "mdb" "sqlite" "nc"))
  ;;   (dired-rainbow-define media "#de751f" ("mp3" "mp4" "mkv" "MP3" "MP4" "avi" "mpeg" "mpg" "flv" "ogg" "mov" "mid" "midi" "wav" "aiff" "flac"))
  ;;   (dired-rainbow-define image "#f66d9b" ("tiff" "tif" "cdr" "gif" "ico" "jpeg" "jpg" "png" "psd" "eps" "svg"))
  ;;   (dired-rainbow-define log "#c17d11" ("log"))
  ;;   (dired-rainbow-define shell "#f6993f" ("awk" "bash" "bat" "sed" "sh" "zsh" "vim"))
  ;;   (dired-rainbow-define interpreted "#38c172" ("py" "ipynb" "rb" "pl" "t" "msql" "mysql" "pgsql" "sql" "r" "clj" "cljs" "scala" "js"))
  ;;   (dired-rainbow-define compiled "#4dc0b5" ("asm" "cl" "lisp" "el" "c" "h" "c++" "h++" "hpp" "hxx" "m" "cc" "cs" "cp" "cpp" "go" "f" "for" "ftn" "f90" "f95" "f03" "f08" "s" "rs" "hi" "hs" "pyc" ".java"))
  ;;   (dired-rainbow-define executable "#8cc4ff" ("exe" "msi"))
  ;;   (dired-rainbow-define compressed "#51d88a" ("7z" "zip" "bz2" "tgz" "txz" "gz" "xz" "z" "Z" "jar" "war" "ear" "rar" "sar" "xpi" "apk" "xz" "tar"))
  ;;   (dired-rainbow-define packaged "#faad63" ("deb" "rpm" "apk" "jad" "jar" "cab" "pak" "pk3" "vdf" "vpk" "bsp"))
  ;;   (dired-rainbow-define encrypted "#ffed4a" ("gpg" "pgp" "asc" "bfe" "enc" "signature" "sig" "p12" "pem"))
  ;;   (dired-rainbow-define fonts "#6cb2eb" ("afm" "fon" "fnt" "pfb" "pfm" "ttf" "otf"))
  ;;   (dired-rainbow-define partition "#e3342f" ("dmg" "iso" "bin" "nrg" "qcow" "toast" "vcd" "vmdk" "bak"))
  ;;   (dired-rainbow-define vc "#0074d9" ("git" "gitignore" "gitattributes" "gitmodules"))
  ;;   (dired-rainbow-define-chmod executable-unix "#38c172" "-.*x.*"))

(map! :leader
      (:prefix ("d" . "dired")
       :desc "Open dired" "d" #'dired
       :desc "Dired jump to current" "j" #'dired-jump)
      (:after dired
       (:map dired-mode-map
        :desc "Peep-dired image previews" "d p" #'peep-dired
        :desc "Dired view file" "d v" #'dired-view-file)))
;; Make 'h' and 'l' go back and forward in dired. Much faster to navigate the directory structure!

(evil-define-key 'normal dired-mode-map
  (kbd "M-RET") 'dired-display-file
  (kbd "h") 'dired-up-directory
  (kbd "l") 'dired-open-file ; use dired-find-file instead of dired-open.
  (kbd "m") 'dired-mark
  (kbd "t") 'dired-toggle-marks
  (kbd "u") 'dired-unmark
  (kbd "C") 'dired-do-copy
  (kbd "D") 'dired-do-delete
  (kbd "J") 'dired-goto-file
  (kbd "M") 'dired-chmod
  (kbd "O") 'dired-chown
  (kbd "P") 'dired-do-print
  (kbd "R") 'dired-rename
  (kbd "T") 'dired-do-touch
  (kbd "Y") 'dired-copy-filenamecopy-filename-as-kill ; copies filename to kill ring.
  (kbd "+") 'dired-create-directory
  (kbd "-") 'dired-up-directory
  (kbd "% l") 'dired-downcase
  (kbd "% u") 'dired-upcase
  (kbd "; d") 'epa-dired-do-decrypt
  (kbd "; e") 'epa-dired-do-encrypt)
;; If peep-dired is enabled, you will get image previews as you go up/down with 'j' and 'k'
(evil-define-key 'normal peep-dired-mode-map
  (kbd "j") 'peep-dired-next-file
  (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)
;; Get file icons in dired
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
;; With dired-open plugin, you can launch external programs for certain extensions
;; For example, I set all .png files to open in 'sxiv' and all .mp4 files to open in 'mpv'
(setq dired-open-extensions '(("gif" . "sxiv")
                              ("jpg" . "sxiv")
                              ("png" . "sxiv")
                              ("mkv" . "mpv")
                              ("mp4" . "mpv")))
(use-package peep-dired
  :defer t ; don't access `dired-mode-map' until `peep-dired' is loaded
  :bind (:map dired-mode-map
         ("P" . peep-dired)))
  (use-package dired-single
    :defer t)

  (use-package dired-ranger
    :defer t)

  (use-package dired-collapse
    :defer t)

  (evil-collection-define-key 'normal 'dired-mode-map
    "H" 'dired-omit-mode
    "y" 'dired-ranger-copy
    "X" 'dired-ranger-move
    "p" 'dired-ranger-paste)

(use-package evil-collection)
(require 'evil-collection)

(map! :leader
      :desc "EWW web browser"
      "e w" #'eww
      :leader
      :desc "EWW reload page"
      "e R" #'eww-reload
      :leader
      :desc "Search web for text between BEG/END"
      "s w" #'eww-search-words)

(use-package dashboard
  :init
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-banner-logo-title "Welcome to Emacs!")
  (setq dashboard-startup-banner 'logo)) ; Makes logo Default banner
  ;; (setq dashboard-startup-banner "~/.config/doom/logo.jpg")  ; use custom image as banner
  (setq dashboard-center-content t)
  (setq dashboard-items '((recents . 5)
                          (agenda . 5 )
                          (bookmarks . 5)
                          (projects . 5)
                          (registers . 5)))

  :config
  (dashboard-setup-startup-hook)
  (dashboard-modify-heading-icons '((recents . "file-text")
                                    (bookmarks . "book")))

(setq gdscript-godot-executable t)

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)
(setq TeX-PDF-mode t)

(load (expand-file-name "~/quicklisp/slime-helper.el"))
(setq inferior-lisp-program "/usr/bin/sbcl")

(use-package rainbow-identifiers
  :hook 'lisp-mode-hook)

(setq package-selected-packages '(clojure-mode lsp-mode cider lsp-treemacs flycheck company))

(when (cl-find-if-not #'package-installed-p package-selected-packages)
  (package-refresh-contents)
  (mapc #'package-install package-selected-packages))

(add-hook 'clojure-mode-hook 'lsp)
(add-hook 'clojurescript-mode-hook 'lsp)
(add-hook 'clojurec-mode-hook 'lsp)

(setq gc-cons-threshold (* 100 1024 1024)
      read-process-output-max (* 1024 1024)
      treemacs-space-between-root-nodes nil
      company-minimum-prefix-length 1
      lsp-lens-enable t
      lsp-signature-auto-activate nil)
      ; lsp-enable-indentation nil ; uncomment to use cider indentation instead of lsp
      ; lsp-enable-completion-at-point nil ; uncomment to use cider completion instead of lsp

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-c l")
  :config
  (lsp-enable-which-key-integration t)
  (add-hook 'rust-mode-hook #'lsp))

(global-set-key (kbd "C-c m") #'completion-at-point)

(require 'ivy-posframe)
;; display at `ivy-posframe-style'
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display)))
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-frame-center)))
(setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-window-center)))
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-frame-bottom-left)))
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-window-bottom-left)))
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-frame-top-center)))
;; (ivy-posframe-mode 1)
;; (setq ivy-posframe-display-functions-alist
;;       '((swiper                     . ivy-posframe-display-at-point)
;;         (complete-symbol            . ivy-posframe-display-at-window-center)
;;         (counsel-M-x                . ivy-posframe-display-at-window-center)
;;         (counsel-esh-history        . ivy-posframe-display-at-window-center)
;;         (counsel-describe-function  . ivy-display-function-fallback)
;;         (counsel-describe-variable  . ivy-posframe-display-at-window-center)
;;         (counsel-find-file          . ivy-display-function-fallback)
;;         (counsel-recentf            . ivy-display-function-fallback)
;;         (counsel-register           . ivy-posframe-display-at-frame-bottom-window-center)
;;         (nil                        . ivy-posframe-display))
;;       ivy-posframe-height-alist
;;       '((swiper . 40)
;;         (t . 10)))
(ivy-posframe-mode 1) ; 1 enables posframe-mode, 0 disables it.

(use-package! elfeed-goodies)
(elfeed-goodies/setup)
(setq elfeed-goodies/entry-pane-size 0.5)
(add-hook 'elfeed-show-mode-hook 'visual-line-mode)
(evil-define-key 'normal elfeed-show-mode-map
  (kbd "J") 'elfeed-goodies/split-show-next
  (kbd "K") 'elfeed-goodies/split-show-prev)
(evil-define-key 'normal elfeed-search-mode-map
  (kbd "J") 'elfeed-goodies/split-show-next
  (kbd "K") 'elfeed-goodies/split-show-prev)
(setq elfeed-feeds (quote
                    (("https://www.reddit.com/r/linux.rss" reddit linux)
                     ("https://www.reddit.com/r/commandline.rss" reddit commandline)
                     ("https://www.reddit.com/r/distrotube.rss" reddit distrotube)
                     ("https://odysee.com/$/rss/@DistroTube:2" odysee distrotube)
                     ("https://www.reddit.com/r/emacs.rss" reddit emacs)
                     ("https://www.gamingonlinux.com/article_rss.php" gaming linux)
                     ("https://hackaday.com/blog/feed/" hackaday linux)
                     ("https://opensource.com/feed" opensource linux)
                     ("https://linux.softpedia.com/backend.xml" softpedia linux)
                     ("https://itsfoss.com/feed/" itsfoss linux)
                     ("https://www.zdnet.com/topic/linux/rss.xml" zdnet linux)
                     ("https://www.phoronix.com/rss.php" phoronix linux)
                     ("http://feeds.feedburner.com/d0od" omgubuntu linux)
                     ("https://www.computerworld.com/index.rss" computerworld linux)
                     ("https://www.networkworld.com/category/linux/index.rss" networkworld linux)
                     ("https://www.techrepublic.com/rssfeeds/topic/open-source/" techrepublic linux)
                     ("https://betanews.com/feed" betanews linux)
                     ("http://lxer.com/module/newswire/headlines.rss" lxer linux)
                     ("https://distrowatch.com/news/dwd.xml" distrowatch linux))))

;; (defun prefer-horizontal-split ()
;;   (set-variable 'split-height-threshold nil t)
;;   (set-variable 'split-width-threshold 50 t))

;; (add-hook 'markdown-mode-hook 'prefer-horizontal-split)
(map! :leader
      :desc "Clone indirect buffer other window"
      "b c" #'clone-indirect-buffer-other-window)


(defun prefer-horizontal-split ()
  (set-variable 'split-height-threshold nil t)
  (set-variable 'split-width-threshold 40 t)) ; make this as low as needed
(add-hook 'markdown-mode-hook 'prefer-horizontal-split)
(map! :leader
      :desc "Clone indirect buffer other window" "b c" #'clone-indirect-buffer-other-window)

;; (use-package fira-code-mode
;   :config (global-fira-code-mode)
  ;; :custom (fira-code-mode-disabled-ligatures '("[]" ":" "x")))
(defconst jetbrains-ligature-mode--ligatures
   '("-->" "//" "/**" "/*" "*/" "<!--" ":=" "->>" "<<-" "->" "<-"
     "<=>" "==" "!=" "<=" ">=" "=:=" "!==" "&&" "||" "..." ".."
     "|||" "///" "&&&" "===" "++" "--" "=>" "|>" "<|" "||>" "<||"
     "|||>" "<|||" ">>" "<<" "::=" "|]" "[|" "{|" "|}"
     "[<" ">]" ":?>" ":?" "/=" "[||]" "!!" "?:" "?." "::"
     "+++" "??" "###" "##" ":::" "####" ".?" "?=" "=!=" "<|>"
     "<:" ":<" ":>" ">:" "<>" "***" ";;" "/==" ".=" ".-" "__"
     "=/=" "<-<" "<<<" ">>>" "<=<" "<<=" "<==" "<==>" "==>" "=>>"
     ">=>" ">>=" ">>-" ">-" "<~>" "-<" "-<<" "=<<" "---" "<-|"
     "<=|" "/\\" "\\/" "|=>" "|~>" "<~~" "<~" "~~" "~~>" "~>"
     "<$>" "<$" "$>" "<+>" "<+" "+>" "<*>" "<*" "*>" "</>" "</" "/>"
     "<->" "..<" "~=" "~-" "-~" "~@" "^=" "-|" "_|_" "|-" "||-"
     "|=" "||=" "#{" "#[" "]#" "#(" "#?" "#_" "#_(" "#:" "#!" "#="
     "&="))

(sort jetbrains-ligature-mode--ligatures (lambda (x y) (> (length x) (length y))))

(dolist (pat jetbrains-ligature-mode--ligatures)
  (set-char-table-range composition-function-table
                      (aref pat 0)
                      (nconc (char-table-range composition-function-table (aref pat 0))
                             (list (vector (regexp-quote pat)
                                           0
                                    'compose-gstring-for-graphic)))))

(defun emacs-run-launcher ()
  "Create and select a frame called emacs-run-launcher which consists only of a minibuffer and has specific dimensions. Run counsel-linux-app on that frame, which is an emacs command that prompts you to select an app and open it in a dmenu like behaviour. Delete the frame after that command has exited"
  (interactive)
  (with-selected-frame (make-frame '((name . "emacs-run-launcher")
                                     (minibuffer . only)
                                     (width . 120)
                                     (height . 11)))
    (unwind-protect
        (counsel-linux-app)
      (delete-frame))))

(defcustom web-page-alist '()
  "Alist used by `emacs-web-page-selector' to associate web-links
  with their names. Needs to be an alist of the form (name
  . link) with both properties being strings. Initialised as an
  empty list as there is no point in predefining anything in it."
  :type 'alist)

(defun emacs-web-page-selector ()
  "Create and select a frame called emacs-web-page-selector which
consists of only a minibuffer and has specific dimensions. Inside
that frame, run a `completing-read' prompting the user to select
the name of a website in the `web-page-alist'. Upon selection,
emacs will run `browse-url' opening the link associated to the
selected name."
  (interactive)
  (with-selected-frame (make-frame '((name . "emacs-web-page-selector")
				     (minibuffer . only)
				     (width . 50)
				     (height . 11)))
    (unwind-protect
	(browse-url
	 (cdr (assoc (completing-read "Web-Page: " web-page-alist) web-page-alist)))
      (delete-frame))))
