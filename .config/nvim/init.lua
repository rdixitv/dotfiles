local os = os
local vim = vim

require "paq" {
	"savq/paq-nvim",
	"neovim/nvim-lsp",
	"folke/tokyonight.nvim",
	"elkowar/yuck.vim",
	"zah/nim.vim",
	"neovimhaskell/haskell-vim",
	"vimwiki/vimwiki",
	"tpope/vim-commentary",
	"scalameta/nvim-metals",
	"elixir-editors/vim-elixir",
	"preservim/vim-markdown",
	"nvim-lualine/lualine.nvim",
	"kyazdani42/nvim-web-devicons",
	"windwp/nvim-autopairs",
	"kyazdani42/nvim-tree.lua",
	"lukas-reineke/indent-blankline.nvim",
	"andweeb/presence.nvim",
	"glepnir/dashboard-nvim",
	"axvr/org.vim",
	"khaveesh/vim-fish-syntax",
	"mlochbaum/BQN",
	"stevearc/vim-arduino",
	"neovim/nvim-lspconfig",
	"jackguo380/vim-lsp-cxx-highlight",
}

require('nvim-autopairs').setup{}
require('lualine').setup {
	options = {
		theme = 'tokyonight'
	}
}

-- LSP

require'lspconfig'.ccls.setup{
  init_options = {
    highlight = {
      lsRanges = true;
    }
  }
}

require 'lspconfig'.rust_analyzer.setup{}
require'lspconfig'.elixirls.setup{
    cmd = { os.getenv("HOME").."/.config/lsp/elixir-ls/language_server.sh" };
}

require 'lspconfig'.arduino_language_server.setup({
	cmd =  {
		-- Required
		"arduino-language-server",
		"-cli-config", "/home/rd/.arduino15/arduino-cli.yaml",
		-- Optional
		"-cli", "/usr/bin/arduino-cli",
		"-clangd", "/usr/bin/clangd"
	}
})
require'lspconfig'.golangci_lint_ls.setup{}

vim.g.tokyonight_style = "night"
vim.g.tokyonight_italic_functions = true
vim.cmd("colorscheme tokyonight")
vim.cmd[[set wrap]]
vim.cmd[[set nu]]
vim.cmd[[set shiftwidth=4]]
vim.cmd[[cmap w!! %!sudo tee > /dev/null %]]

