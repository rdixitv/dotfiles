(in-package #:nyxt-user)

;; Import Files
(dolist (file (list (nyxt-init-file "statusline.lisp")))
                    ; (nyxt-init-file "stylesheet.lisp")))
  (load file))

(define-configuration (prompt-buffer)
  ((default-modes `(vi-insert-mode ,@%slot-default%))))

(define-configuration (buffer internal-buffer editor-buffer)
  ((default-modes `(vi-normal-mode ,@%slot-default%))))
