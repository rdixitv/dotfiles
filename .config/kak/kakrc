source "%val{config}/plugins/plug.kak/rc/plug.kak"
plug "andreyorst/plug.kak" noload

#plug "andreyorst/powerline.kak" defer powerline_gruvbox %{
#    powerline-theme desertex
#} config %{
#    powerline-start
#}
#
#plug "alexherbo2/auto-pairs.kak" %{
#    enable-auto-pairs
#}
#
#plug "kak-lsp/kak-lsp" do %{
#    cargo install --locked --force --path .
#}

alias global bu buffer

add-highlighter global/ number-lines -hlcursor
add-highlighter global/ wrap
add-highlighter global/ show-matching

set-option global tabstop 4
set-option global indentwidth 4

hook global InsertChar \t %{
    exec -draft h@
}

# Keybindings
hook global NormalKey <ret> w
map global normal D '<a-l>d' -docstring 'delete to end of line'
map global normal Y '<a-l>y' -docstring 'yank to end of line'
map global normal <a-h> Gi
map global prompt <a-i> "<home>(?i)<end>"
map global user E -docstring 'lsp ' ': enter-user-mode lsp<ret>'


# clipboard interaction
map global user p -docstring 'paste from clipboard' ': paste<ret>'
map global user y -docstring 'copy to clipboard' '<a-|>pbcopy<ret>'
map global user d -docstring 'cut to clipboard' '|pbcopy<ret>'

# Comment
define-command comment %{
    try %{
        execute-keys _
        comment-block
    } catch comment-line
}
hook global NormalKey <space> comment

hook global InsertCompletionShow .* %{
    try %{
        execute-keys -draft 'h<a-K>\h<ret>'
        map window insert <tab> <c-n>
        map window insert <s-tab> <c-p>
        hook -once -always window InsertCompletionHide .* %{
            map window insert <tab> <tab>
            map window insert <s-tab> <s-tab>
        }
    }
}

# LSP
hook global WinSetOption filetype=(rust|python|go|c|cpp|haskell|elixir|bash|lua|nim) %{
        lsp-enable-window
}


colorscheme tokyonight
