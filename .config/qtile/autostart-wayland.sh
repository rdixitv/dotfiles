#!/usr/bin/env bash

swaybg -m fill -i "$HOME/wallpapers/pics/TokyoNight/tux.png" &
lxsession &
emacs --daemon &
volumeicon &
ibus-daemon &
nm-applet &
