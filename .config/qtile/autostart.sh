#!/usr/bin/env bash

xrandr -s 1920x1200
lxsession &
picom &
/usr/bin/emacs --daemon &
nm-applet &
feh -zr --bg-fill --no-fehbg /home/rd/wallpapers/pics/fav &
dunst &
volumeicon &
ibus-daemon &
sxhkd &
