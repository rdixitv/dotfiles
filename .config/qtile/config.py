#!/usr/bin/env python

# -*- coding: utf-8 -*-

import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen, ScratchPad, DropDown
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from typing import List  # noqa: F401from typing import List  # noqa: F401
from libqtile.command.client import InteractiveCommandClient



if qtile.core.name == "x11":
    run = "rofi -show drun -show-icons"
    run2 = "dmenu_run"
elif qtile.core.name == "wayland":
    run = "wofi --show=drun"
    run2 = "wofi --show=run"

myBrowser = "brave"
mod = "mod4"              # Sets mod key to SUPER/WINDOWS
myTerm = "alacritty"      # My terminal of choice

# This first part is kind of unnecessary, but for those who need it, it is there.
colorBack = "#1a1b26" # Bg
colorFore = "#a9b1d6" # Fg
color01 = "#1c1f24" # Dark Grey
color02 = "#f7768e" # Red
color03 = "#92ce6a" # Green
color04 = "#ff9364" # Orange
color05 = "#51afef" # Blue
color06 = "#bb9af7" # Purple
color07 = "#5699af" # Green-blue
color08 = "#202328" # Black
color09 = "#5b6268" # Grey
color10 = "#4db5bd" # Aqua
color11 = "#ecbe7b" # Beige
color12 = "#3071db" # Dark blue
color13 = "#73daca" # Light Purple
color14 = "#b4f9f8" # Light blue
color15 = "#a9b1d6" # White
#colors = [[colorBack, colorBack], # panel background
#          ["#3d3f4b", "#434758"], # background for current screen tab
#          ["#ffffff", "#ffffff"], # font color for group names
#          ["#ff5555", "#ff5555"], # border line color for current tab
#          ["#ff9364", "#ff9364"], # border line color for 'other tabs' and color for 'odd widgets'
#          [color07, color07], # color for the 'even widgets'
#          [colorFore, colorFore], # window name
#          ["#ecbbfb", "#ecbbfb"]] # backbround for inactive screens

colors = [["#1a1b26", "#1a1b26"],
          ["#1c1f24", "#1c1f24"],
          ["#a9b1d6", "#a9b1d6"],
          ["#f7768e", "#f7768e"],
          ["#92ce6a", "#92ce6a"],
          ["#ff9364", "#ff9364"],
          ["#7dcfff", "#7dcfff"],
          ["#bb9af7", "#bb9af7"],
          ["#b4f9f8", "#b4f9f8"],
          ["#73daca", "#73daca"],
          ["#999999", "#999999"]]
# START_KEYS
keys = [
         ### The essentials
         Key([mod], "Return",
             lazy.spawn(myTerm),
             desc='Launches My Terminal'
             ),
         Key([mod, "shift"], "Return",
             lazy.spawn(run),
             desc='Run Launcher'
             ),
         Key([mod, "mod1"], "Return",
             lazy.spawn(run2),
             desc='Run Launcher 2'
             ),
         Key([mod, "mod1"], "Tab",
             lazy.spawn("rofi -show window"),
             ),
         Key(["mod1"], "Tab",
             lazy.next_layout(),
             desc='Toggle through layouts'
             ),
         Key(["mod1", "shift"], "Tab",
             lazy.prev_layout(),
             desc='Toggle through layouts'
             ),
         Key([mod], "Tab",
             lazy.screen.next_group(),
             desc='Switch to next group'
             ),
         Key([mod, "shift"], "Tab",
             lazy.screen.prev_group(),
             desc='Switch to previous group'
             ),
         Key([mod, "shift"], "c",
             lazy.window.kill(),
             desc='Kill active window'
             ),
         Key([mod, "shift"], "r",
             lazy.restart(),
             desc='Restart Qtile'
             ),
         Key([mod, "shift"], "q",
             lazy.shutdown(),
             desc='Shutdown Qtile'
             ),
         ### Switch focus to specific monitor (out of three)
         Key([mod, "mod1"], "1",
             lazy.to_screen(0),
             desc='Keyboard focus to monitor 1'
             ),
         Key([mod, "mod1"], "2",
             lazy.to_screen(1),
             desc='Keyboard focus to monitor 2'
             ),
         Key([mod, "mod1"], "3",
             lazy.to_screen(2),
             desc='Keyboard focus to monitor 3'
             ),
         ### Switch focus of monitors
         Key([mod], "period",
             lazy.next_screen(),
             desc='Move focus to next monitor'
             ),
         Key([mod], "comma",
             lazy.prev_screen(),
             desc='Move focus to prev monitor'
             ),
         ### Treetab controls
          Key([mod, "shift"], "h",
             lazy.layout.move_left(),
             desc='Move up a section in treetab'
             ),
         Key([mod, "shift"], "l",
             lazy.layout.move_right(),
             desc='Move down a section in treetab'
             ),
         ### Window controls
         Key([mod], "h",
             lazy.layout.left(),
             desc='Move focus left in current stack pane'
             ),
         Key([mod], "j",
             lazy.layout.down(),
             desc='Move focus down in current stack pane'
             ),
         Key([mod], "k",
             lazy.layout.up(),
             desc='Move focus up in current stack pane'
             ),
         Key([mod], "l",
             lazy.layout.right(),
             desc='Move focus right in current stack pane'
             ),
         Key([mod, "shift"], "j",
             lazy.layout.shuffle_down(),
             lazy.layout.section_down(),
             desc='Move windows down in current stack'
             ),
         Key([mod, "shift"], "k",
             lazy.layout.shuffle_up(),
             lazy.layout.section_up(),
             desc='Move windows up in current stack'
             ),
         Key([mod, "shift"], "h",
             lazy.layout.shuffle_left(),
             lazy.layout.section_left(),
             desc='Move windows left in current stack'
             ),
         Key([mod, "shift"], "l",
             lazy.layout.shuffle_right(),
             lazy.layout.section_right(),
             desc='Move windows right in current stack'
             ),
         Key([mod, "mod1"], "h",
             lazy.layout.shrink(),
             lazy.layout.decrease_nmaster(),
             desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
             ),
         Key([mod, "mod1"], "l",
             lazy.layout.grow(),
             lazy.layout.increase_nmaster(),
             desc='Expand window (MonadTall), increase number in master pane (Tile)'
             ),
         Key([mod, "mod1"], "n",
             lazy.layout.normalize(),
             desc='normalize window size ratios'
             ),
         Key([mod], "m",
             lazy.layout.maximize(),
             desc='toggle window between minimum and maximum sizes'
             ),
         Key([mod], "t",
             lazy.window.toggle_floating(),
             desc='toggle floating'
             ),
         Key([mod], "space",
             lazy.window.toggle_fullscreen(),
             desc='toggle fullscreen'
             ),
         ### Stack controls
         Key([mod, "mod1", "shift"], "Tab",
             lazy.layout.rotate(),
             lazy.layout.flip(),
             desc='Switch which side main pane occupies (XmonadTall)'
             ),
          Key([mod, "mod1"], "space",
             lazy.layout.next(),
             desc='Switch window focus to other pane(s) of stack'
             ),
         Key([mod, "mod1", "shift"], "space",
             lazy.layout.toggle_split(),
             desc='Toggle between split and unsplit sides of stack'
             ),
        ### Media
        # Key([], "XF86AudioLowerVolume", 
        #         lazy.spawn("pamixer -d 5"),
                # desc="Decrease volume"
                # ),
        # Key([], "XF86AudioRaiseVolume", 
        #         lazy.spawn("pamixer -i 5"),
                # desc="Increase volume"
                # ),
        # Key([], "XF86AudioMute",
        #         lazy.spawn("pamixer -t"),
                # desc="Mute volume"
                # ),
        Key([], "XF86AudioPlay", 
                lazy.spawn("playerctl play-pause"), 
                desc="Pause track"
                ),
        Key([], "XF86AudioNext", 
                lazy.spawn("playerctl next"),
                desc="Next track"
                ),
        Key([], "XF86AudioPrev", 
                lazy.spawn("playerctl previous"),
                desc="Previous track"
                ),
        Key([], "XF86MonBrightnessUp", 
                lazy.spawn("xbacklight -inc 10"), 
                desc="Increase screen brightness"
                ),
        Key([], "XF86MonBrightnessDown", 
                lazy.spawn("xbacklight -dec 10"),
                desc="Decrease screen brightness"
                ),

        Key([mod, "mod1"], "slash",
                lazy.spawn("feh -zr --bg-fill --no-fehbg /home/rd/wallpapers/pics/fav &"),
                desc='Randomize wallpaper'
                ),
        Key([mod, "mod1", "shift"], "slash",
                lazy.spawn("feh -zr --bg-fill --no-fehbg /home/rd/wallpapers/pics/fav/TokyoNight &"),
                desc='Randomize wallpaper to simple'
                ),
		### Scratchpad
		Key([mod, "mod1"], "t",
				lazy.group['Scratchpad'].dropdown_toggle('term'),
				desc='Terminal scratcpad'
				),
		Key([mod, "mod1"], "m",
				lazy.group['Scratchpad'].dropdown_toggle('music'),
				desc='Music scratcpad'
				),
		Key([mod, "mod1"], "f",
				lazy.group['Scratchpad'].dropdown_toggle('fmger'),
				desc='GUI file manager scratcpad'
				),
		Key([mod, "mod1", "shift"], "f",
				lazy.group['Scratchpad'].dropdown_toggle('fm'),
				desc='Terminal file manager scratcpad'
				),
		Key([mod, "mod1"], "c",
				lazy.group['Scratchpad'].dropdown_toggle('calc'),
				desc='Calculator scratcpad'
				),

        ### Help
        Key([mod], "slash",
                lazy.spawn("/home/rd/.config/qtile/qtile-keys"),
                desc='View Keybindings'
                ),
        Key([mod], "d",
                lazy.spawncmd(),
                desc='Run Launcher'),
        Key([mod, "shift"], "b", 
                lazy.hide_show_bar(), 
                desc="Toggle bar visibility"
                ),
]
# END_KEYS

groups = [Group("DEV", layout='monadthreecol'),
          Group("WWW", layout='monadtall'),
          Group("SYS", layout='ratiotile'),
          Group("SCH", layout='monadtall'),
          Group("BASE", layout='max'),
          Group("BG", layout='monadtall'),
          Group("MEET", layout='max'),
          Group("EDIT", layout='monadtall'),
          Group("IRC", layout='ratiotile'),
          Group("MISC", layout='ratiotile'),
				  ] 

groups.append(ScratchPad('Scratchpad', [
    DropDown("term", myTerm,
             width=0.75, height=0.9,
             x=0.125, y=0.05, opacity=1),
	DropDown("music", myTerm + ' -e ncmpcpp',
             width=0.75, height=0.9,
             x=0.125, y=0.05, opacity=1),
    DropDown("fmger", "pcmanfm-qt",
             width=0.75, height=0.85,
             x=0.125, y=0.05, opacity=1),
    DropDown("fm", myTerm+' -e lf',
             width=0.75, height=0.85,
             x=0.125, y=0.075, opacity=1),
	DropDown("calc", "qalculate-gtk",
             width=0.3, height=0.1,
             x=0.6, y=0.55, opacity=1),
]))

# groups = [Group("", layout='monadtall'),
#           Group("", layout='monadtall'),
#           Group("", layout='monadtall'),
#           Group("拾", layout='ratiotile'),
#           Group("", layout='max'),
#           Group("", layout='monadtall'),
#           Group("", layout='max'),
#           Group("", layout='monadtall'),
#           Group("", layout='ratiotile')],
# Allow MODKEY+[0 through 9] to bind to groups, see https://docs.qtile.org/en/stable/manual/config/groups.html
# MOD4 + index Number : Switch to Group[index]
# MOD4 + shift + index Number : Send active window to another Group
from libqtile.dgroups import simple_key_binder
dgroups_key_binder = simple_key_binder("mod4")


layout_theme = {"border_width": 2,
                # "fullscreen_border_width": 0,
                "margin": 6,
                "border_focus": "BB9AF7",
                "border_normal": "414868",
                }

layouts = [
    layout.MonadWide(**layout_theme),
    layout.Tile(shift_windows = True, **layout_theme),
    layout.VerticalTile(**layout_theme),
    layout.Zoomy(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    layout.Stack(num_stacks=2),
    layout.RatioTile(**layout_theme),
	layout.MonadThreeCol(
		new_client_position = 'after_current', 
		main_centered = False,
		ratio = 0.45,
		**layout_theme),
    layout.TreeTab(
         font = "Ubuntu",
         fontsize = 16,
         sections = ["I", "II", "III", "IV", "V"],
         section_fontsize = 10,
         border_width = 2,
         bg_color = colorBack,
         active_bg = "bb9af7",
         active_fg = "000000",
         inactive_bg = "73daca",
         inactive_fg = "1c1f24",
         padding_left = 0,
         padding_x = 0,
         padding_y = 5,
         section_top = 10,
         section_bottom = 20,
         level_shift = 8,
         vspace = 3,
         panel_width = 200
         ),
    layout.Floating(**layout_theme)
]

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="Ubuntu Nerd Font Bold",
    fontsize = 16,
    padding = 2,
    background=colors[2]
)
extension_defaults = widget_defaults.copy()


def init_widgets_list():
    widgets_list = [
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              # widget.Image(
              #           filename = "~/.config/qtile/icons/python-white.png",
              #           scale = "False",
              #           mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm)}
              #          ),

              widget.CurrentLayoutIcon(
                       custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                       foreground = colors[2],
                       background = colors[0],
                       padding = 0,
                       scale = 0.7
                       ),
            widget.GroupBox(
                font = 'Ubuntu Bold',
                fontsize = 21,
                highlight_method = 'block',
                background = colors[0],
                inactive = colors[10],
                active = colors[4],
                padding_y = 7,
                rounded = "true"
            ), 
              # widget.GroupBox(
              #          font = "Ubuntu Bold",
              #          fontsize = 16,
              #          margin_y = 3,
              #          margin_x = 0,
              #          padding_y = 5,
              #          padding_x = 3,
              #          borderwidth = 3,
              #          active = colors[6],
              #          inactive = colors[10],
              #          rounded = False,
              #          highlight_color = colors[1],
              #          highlight_method = "line",
              #          this_current_screen_border = colors[7],
              #          this_screen_border = colors [4],
              #          other_current_screen_border = colors[8],
              #          other_screen_border = colors[4],
              #          foreground = colors[2],
              #          background = colors[0]
              #          ),
              widget.Prompt(
                  background = colors[0],
                  cursor_blink = 0,
                  prompt = '↪ ',
                  bell_style = "visual",
                  visual_bell_color = colors[3],
				  font = "JetBrainsMono NF",
				  fontsize = 21,
                  ),
             widget.WindowName(
                       foreground = colors[4],
                       background = colors[0],
                       font = "JetBrainsMono NF",
                       fontsize = 21,
                       padding = 6
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[0],
                       background = colors[0]
                       ),
              widget.TextBox(
                       text = '',
                       font = "Ubuntu Bold",
                       background = colors[0],
                       foreground = colors[4],
                       padding = -25,
                       fontsize = 121
                       ),

            widget.WidgetBox(widgets=[
                widget.TextBox(
                       text = '',
                       font = "Ubuntu Bold",
                       background = colors[0],
                       foreground = colors[4],
                       padding = -25,
                       fontsize = 121
                       ),

              widget.TextBox(
                       text = '',
                       font = "Ubuntu Nerd Font",
                       background = colors[0],
                       foreground = colors[4],
                       padding = 4,
                       fontsize = 30,
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("emacsclient -c -a 'emacs' --eval '(doom/window-maximize-buffer(cfw:open-org-calendar))'")},
                       ),
              widget.Clock(
                       foreground = colors[4],
                       background = colors[0],
                       format = '%a, %b %d, %Y',
                       fontsize = 20,
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("emacsclient -c -a 'emacs' --eval '(doom/window-maximize-buffer(cfw:open-org-calendar))'")},
                       ),
              widget.TextBox(
                       text = '',
                       font = "Ubuntu Bold",
                       background = colors[0],
                       foreground = colors[4],
                       padding = -25,
                       fontsize = 121
                       ),
              # widget.TextBox(
              #          text = '',
              #          font = "Ubuntu Nerd Font",
              #          background = colors[0],
              #          foreground = colorFore,
              #          padding = 0,
              #          fontsize = 30
              #          ),
              # widget.CheckUpdates(
              #         background = colors[0],
              #         foreground = colorFore,
              #         custom_command = "echo ' '; /home/rd/.local/bin/mycheckupdates",
              #         no_update_string = " 0 ",
              #         restart_indicator = "",
              #         fmt = "{}",
              #         mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e doas pacman -Syu'), 'Button3': lambda: qtile.cmd_spawn(myTerm + ' -e doas pacman -Syyu')},
              #         fontsize = 24
              # ),
              # widget.TextBox(
              #          text = '',
              #          font = "Ubuntu Nerd Font",
              #          background = colors[0],
              #          foreground = colors[10],
              #          padding = 0,
              #          fontsize = 30
              #          ),


                widget.CPUGraph(
                  background = colors[4],
                  fill_color = colors[0],
                  border_color = colors[4],
                  graph_color = colors[0],
                  mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e btop'), 'Button3': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                  ),
              widget.TextBox(
                       text = '',
                       font = "Ubuntu Bold",
                       background = colors[4],
                       foreground = colors[0],
                       padding = -25,
                       fontsize = 121
                       ),
                 widget.MemoryGraph(
                  background = colors[0],
                  fill_color = colors[4],
                  border_color = colors[0],
                  graph_color = colors[4],
                  mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e btop'), 'Button3': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                  ),
              widget.TextBox(
                       text = '',
                       font = "Ubuntu Bold",
                       background = colors[0],
                       foreground = colors[4],
                       padding = -25,
                       fontsize = 121
                       ),
                ],

                background = colors[4],
                text_closed = "",
                text_open = "",
                fontsize = 30,
                padding = 0,
                foreground = colors[0],
                close_button_location = 'right'
            ),
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[0],
                       background = colors[4]
                       ),
              widget.TextBox(
                       text = '',
                       font = "Ubuntu Bold",
                       background = colors[4],
                       foreground = colors[0],
                       padding = -25,
                       fontsize = 121
                       ),

              widget.NetGraph(
                  background = colors[0],
                  fill_color = colors[4],
                  border_color = colors[0],
                  graph_color = colors[4],
                  mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e speedtest'), 'Button3': lambda: qtile.cmd_spawn(myTerm + ' -e btop')},
                  ),
#             widget.Net(
#                       interface = "wlp3s0",
#                       format = '直 {down} ↓↑ {up}',
#                       foreground = colors[3],
#                       background = colors[0],
#                       padding = 5,
#                       ),
              widget.TextBox(
                       text = '',
                       font = "Ubuntu Bold",
                       background = colors[0],
                       foreground = colors[4],
                       padding = -25,
                       fontsize = 121
                       ),
              widget.Battery(
                       update_interval = 1,
                       format = '{char} {percent:2.0%}',
                       full_char = '',
                       charge_char = ' ',
                       discharge_char = ' ',
                       foreground = colors[0],
                       padding = 4,
                       background = colors[4],
                       fontsize = 20,
                       font = "Ubuntu Nerd Font Bold",
					   mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e btop'), 'Button3': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                       ),

              # widget.TextBox(
              #          text = '',
              #          font = "Ubuntu Nerd Font",
              #          background = colors[0],
              #          foreground = colors[10],
              #          padding = 0,
              #          fontsize = 30
              #          ),
              widget.TextBox(
                       text = '',
                       font = "Ubuntu Bold",
                       background = colors[4],
                       foreground = colors[0],
                       padding = -25,
                       fontsize = 121
                       ),
              widget.TextBox(
                       text = '',
                       font = "Ubuntu Nerd Font",
                       background = colors[0],
                       foreground = colors[4],
                       padding = 0,
                       fontsize = 35
                       ),
              widget.KeyboardLayout(
                mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('setxkbmap intl'), 'Button3': lambda: qtile.cmd_spawn('setxkbmap hmk')},
               configured_keyboards = ["intl", "wmn", "hmk", "dk", "fi", "in kn", "in hi", "us"],
               display_map = {'intl': 'int', 'wmn': 'wkmn', "hmk": "hal", "dk": "dk", "fi": "fi", "in kan": "kn", "in hin": "hi", "us": "us"},
               foreground = colors[4],
               background = colors[0],
               fontsize = 20,
               padding = 4
              ),
              widget.TextBox(
                       text = '',
                       font = "Ubuntu Bold",
                       background = colors[0],
                       foreground = colors[4],
                       padding = -25,
                       fontsize = 121
                       ),
              widget.TextBox(
                       text = '',
                       font = "JetBrainsMono NF",
                       background = colors[4],
                       foreground = colors[0],
                       padding = 4,
                       fontsize = 30,
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("emacsclient -c -a 'emacs' --eval '(doom/window-maximize-buffer(cfw:open-org-calendar))'")},
                       ),
              widget.Clock(
                       foreground = colors[0],
                       background = colors[4],
                       format = "%H:%M:%S",
                       fontsize = 20,
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("emacsclient -c -a 'emacs' --eval '(doom/window-maximize-buffer(cfw:open-org-calendar))'")},
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[0],
                       background = colors[4]
                       ),
              widget.TextBox(
                       text = '',
                       font = "Ubuntu Bold",
                       background = colors[4],
                       foreground = colors[0],
                       padding = -25,
                       fontsize = 121
                       ),
              widget.Systray(
                       background = colors[0],
                       fontsize = 30,
                       padding = 0
                       ),
              ]
    return widgets_list

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
#    del widgets_screen1[7:8]               # Slicing removes unwanted widgets (systray) on Monitors 1,3
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2                 # Monitor 2 will display all widgets in widgets_list

def init_widgets_wayland():
    widgets_wl = init_widgets_list
    del widgets_wl[24:25]
    return widgets_wl

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=40))]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()

    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(
    border_focus = colors[6],
    border_normal = "414868",
    border_width = 2,
    float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    # default_float_rules include: utility, notification, toolbar, splash, dialog,
    # file_progress, confirm, download and error.
    *layout.Floating.default_float_rules,
    Match(title='Confirmation'),      # tastyworks exit box
    Match(title='Qalculate!'),        # qalculate-gtk
    Match(wm_class='pinentry-gtk-2'), # GPG key password entry
    Match(wm_class='Yad'),
    Match(wm_class='pavucontrol')
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True
single_border_width = 0
auto_minimize = True

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    if qtile.core.name == "x11":
        subprocess.call([home + '/.config/qtile/autostart.sh'])
    elif qtile.core.name == "wayland":
        subprocess.call([home + '/.config/qtile/autostart-wayland.sh'])

wmname = "LG3D"
# vim: tabstop=4 shiftwidth=4 noexpandtab

