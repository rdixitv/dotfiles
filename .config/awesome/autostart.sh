#!/usr/bin/env bash

xrandr -s 1920x1200 &
sxhkd &
killall volumeicon cbatticon
volumeicon 
cbatticon
nm-applet &
feh -zr --no-fehbg --bg-fill "$HOME/wallpapers/pics/fav" &
dunst &
flameshot &
picom &
emacs --daemon &
lxsession &
# dunst &
