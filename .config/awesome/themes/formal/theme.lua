local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local lain = require("lain")
local naughty = require("naughty")
local dpi = require("beautiful.xresources").apply_dpi
local math, string, os = math, string, os
local my_table = awful.util.table or gears.table

-- Theme Definition {{{
local theme = {}
theme.dir = os.getenv("HOME").."/.config/awesome/themes/formal/"
theme.dir2 = os.getenv("HOME").."/.config/awesome/themes/one-dark"
-- Fonts
theme.font_name = "JetBrainsMono NF"

theme.font = "JetBrainsMono NF 20"
theme.taglist_font = "JetBrainsMono NF 30"
theme.icon_font = "JetBrainsMono NF 16"
theme.widget_font = 'JetBrainsMono 20'
theme.notification_font = "JetBrainsMono NF 12"
theme.tasklist_font = "JetBrainsMono NF 20"

theme.menu_height                               = 20
theme.menu_width                                = 140
theme.menu_submenu_icon  = "/home/rd/.config/awesome/themes/one-dark/icons/submenu.png"
-- colors
theme.clr = {
	purple = "#c678dd",
	blue = "#51afef",
	green = "#98be65",
	red = '#ff6c6b',
	gray = '#5b6268',
    orange = '#da8548',
	yellow = '#ecbe7b'
}
theme.fg_normal = '#bbc2cf'
theme.fg_focus = '#c678dd'
theme.fg_urgent = '#b74822'

theme.bg_normal = '#282c34'
theme.bg_focus = '#c678dd'
theme.bg_urgent = "#3f3f3f"
theme.bg_systray = theme.bg_normal
theme.systray_icon_spacing = dpi(10)

theme.tasklist_bg_normal = '#282838'
theme.tasklist_bg_focus = '#2f2f3f'
theme.tasklist_bg_urgent = '#2b2b3b'

theme.prompt_fg = '#bbc2cf'

theme.taglist_bg_focus = theme.bg_light
theme.taglist_fg_occupied = theme.clr.blue
theme.taglist_fg_urgent = theme.clr.red
theme.taglist_fg_empty = theme.clr.gray
theme.taglist_fg_focus = theme.clr.purple

theme.notification_fg = '#a6accd'
theme.notification_bg = '#222632'
theme.notification_opacity = 1

theme.border_normal = "#666666"
theme.border_focus = '#c678dd'
theme.border_marked = "#51afef"
theme.border_width = 2

theme.tasklist_plain_task_name = true
theme.tasklist_disable_icon = true
theme.useless_gap = 4;
theme.gap_single_client = false


theme.layout_fairh = theme.dir.."layouts/fairhw.png"
theme.layout_fairv = theme.dir.."layouts/fairvw.png"
theme.layout_floating  = theme.dir.."layouts/floatingw.png"
theme.layout_magnifier = theme.dir.."layouts/magnifierw.png"
theme.layout_max = theme.dir.."layouts/maxw.png"
theme.layout_fullscreen = theme.dir.."layouts/fullscreenw.png"
theme.layout_tilebottom = theme.dir.."layouts/tilebottomw.png"
theme.layout_tileleft   = theme.dir.."layouts/tileleftw.png"
theme.layout_tile = theme.dir.."layouts/tilew.png"
theme.layout_tiletop = theme.dir.."layouts/tiletopw.png"
theme.layout_spiral  = theme.dir.."layouts/spiralw.png"
theme.layout_dwindle = theme.dir.."layouts/dwindlew.png"
theme.layout_cornernw = theme.dir.."layouts/cornernww.png"
theme.layout_cornerne = theme.dir.."layouts/cornernew.png"
theme.layout_cornersw = theme.dir.."layouts/cornersww.png"
theme.layout_cornerse = theme.dir.."layouts/cornersew.png"
theme.widget_cpu = theme.dir2.."icons/cpu.png"

-- naughty configuration
theme.notification_border_color = theme.bg_light
theme.notification_border_width = dpi(5)

naughty.config.padding = dpi(8)
naughty.config.defaults = {
	timeout = 5,
	text = "",
	ontop = true,
	position = "top_right",
	margin = dpi(10),
}

-- }}}


-- Separators
local spr = wibox.widget.textbox('     ')
local half_spr = wibox.widget.textbox('  ')
local sep = wibox.widget.textbox('<span color="#666666"> | </span>')

-- widgets {{{
local tux = wibox.widget{
    markup = '',
    --markup = '  ',
    align  = 'center',
    valign = 'center',
    font = "JetBrainsMono NF 22",
    widget = wibox.widget.textbox
}




local function line(color)
	return (
		wibox.container.background(
			wibox.widget.textbox(
				'<span font="'..theme.widget_font..'" color="'..color..'">@</span>'
			),
			color
		)
	)
end

-- Clock
local clockicon = wibox.widget.textbox(
	string.format('<span color="%s" font="'..theme.icon_font..'"></span>', theme.clr.purple)
)
local clock = wibox.widget.textclock(
	'<span font="'..theme.widget_font..'" color="'..theme.clr.purple..'"> %X</span>', 1
--  function(widget, stdout)
 --   widget:set_markup(stdout)
--  end
)

local clock =
	{
		layout = wibox.layout.fixed.horizontal,
		clockicon,
		clock,
    sep
	}

-- Calendar
local calendaricon = wibox.widget.textbox(
	string.format('<span color="%s" font="'..theme.icon_font..'"></span>', theme.clr.yellow)
)
local calendar = wibox.widget.textclock(
	'<span font="'..theme.widget_font..'" color="'..theme.clr.yellow..'"> %x (%a)</span>'
)

local calendar =
	{
		layout = wibox.layout.fixed.horizontal,
		calendaricon,
		calendar,
    sep
	}





-- Battery
local baticon = wibox.widget.textbox('')
function theme.update_baticon(icon)
	baticon:set_markup(
		string.format('<span color="%s" font="'..theme.icon_font..'">%s</span>', theme.clr.blue, icon)
	)
end

local bat = wibox.widget.textbox('')
function theme.update_battery()
	awful.spawn.easy_async_with_shell(
	[[bash -c "echo $(acpi|awk '{split($0,a,", "); print a[2]}')"]],
	function(stdout)
		if stdout == '' then
			bat:set_markup('<span color="'..theme.clr.blue..'" font="'..theme.widget_font..'"> N/A</span>')
			return
		end
		stdout = stdout:gsub("%%", ""):match("^%s*(.-)%s*$")
		percent = tonumber(stdout)
		if percent <= 5 then
			theme.update_baticon('')
		elseif percent <= 25 then
			theme.update_baticon('')
		elseif percent >= 25 and percent <= 75 then
			theme.update_baticon('')
		elseif percent < 100 then
			theme.update_baticon('')
		else
			theme.update_baticon('')
		end
			
		bat:set_markup('<span color="'..theme.clr.blue..'" font="'..theme.widget_font..'"> ' ..stdout.."%</span>")
	end)
end
theme.update_battery()

local battery =
	{
		layout = wibox.layout.fixed.horizontal,
		baticon,
		bat,
    sep
	}



-- Updates
local upicon = wibox.widget.textbox('<span color="'..theme.clr.orange..'"></span>')
local up = wibox.widget.textbox('')
theme.updates = function()
	awful.spawn.easy_async_with_shell("/home/rd/.local/bin/check_updates", function(stdout)
		stdout = tostring(stdout)
		number = tonumber(stdout)
		up:set_markup('<span color="'..theme.clr.orange..'" font="'..theme.widget_font..'"> '..stdout.."</span> ")
	end)
end
theme.updates()
local updates =
	{
		layout = wibox.layout.fixed.horizontal,
		upicon,
		up,
		sep,
	}

-- ALSA volume
local volicon = wibox.widget.textbox('')
local vol = wibox.widget.textbox('')
theme.update_volume = function()
	awful.spawn.easy_async_with_shell([[
		if amixer get Master | grep -q '\[on\]'; then
			grep 'Left:' <(amixer sget Master)|awk -F"[][]" '{ print $2 }'
		else
			echo 'muted'
		fi
	]], function(stdout)
		stdout = tostring(stdout):gsub("\n", ''):gsub("%%", ""):match("^%s*(.-)%s*$")
		percent = tonumber(stdout)
		if stdout == "muted" then
			volicon:set_markup('<span color="'..theme.clr.green..'" font="'..theme.icon_font..'">婢</span>')
		elseif percent == 0 then
			volicon:set_markup('<span color="'..theme.clr.green..'" font="'..theme.icon_font..'"></span>')
		elseif percent <= 50 then
			volicon:set_markup('<span color="'..theme.clr.green..'" font="'..theme.icon_font..'">奔</span>')
		else
			volicon:set_markup('<span color="'..theme.clr.green..'" font="'..theme.icon_font..'">墳</span>')
		end
		vol:set_markup('<span color="'..theme.clr.green..'" font="'..theme.widget_font..'"> '..stdout.."%</span>")
	end)
end
theme.update_volume()

local volume =
	{
		layout = wibox.layout.fixed.horizontal,
		volicon,
		vol,
		sep,
	}

-- load theme options {{{
function theme.at_screen_connect(s)
	gears.wallpaper.maximized(theme.dir.."/tile.jpeg", s)

	-- Tags
	awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])

	-- Create a promptbox for each screen
	s.mypromptbox = awful.widget.prompt()
	-- Create an imagebox widget which will contains an icon indicating which layout we're using.
	-- We need one layoutbox per screen.
	s.mylayoutbox = awful.widget.layoutbox(s)
	s.mylayoutbox:buttons(my_table.join(awful.button({}, 1, function()
		awful.layout.inc(1)
	end), awful.button({}, 2, function()
		awful.layout.set(awful.layout.layouts[1])
	end), awful.button({}, 3, function()
		awful.layout.inc(-1)
	end), awful.button({}, 4, function()
		awful.layout.inc(1)
	end), awful.button({}, 5, function()
		awful.layout.inc(-1)
	end)))

    s.mylayoutbox:buttons(my_table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
	-- Create a taglist widget
	s.mytaglist = awful.widget.taglist {
		screen = s,
		filter = awful.widget.taglist.filter.all,
		widget_template = {
			{
				{
					id = 'text_role',
					widget = wibox.widget.textbox,
				},
				layout = wibox.layout.align.horizontal,
			},
			left = 10,
			right = 10,
			widget = wibox.container.margin,
		},
		buttons = awful.util.taglist_buttons
	}
	-- Create a tasklist widget
	s.mytasklist = awful.widget.tasklist {
		screen = s,
		filter = awful.widget.tasklist.filter.currenttags,
		buttons = awful.util.tasklist_buttons,
		widget_template = {
			{
				wibox.widget.base.make_widget(),
				forced_height = 5,
				id            = 'background_role',
				widget        = wibox.container.background,
			},
			{
				{
					id     = 'clienticon',
					widget = awful.widget.clienticon,
				},
				margins = 5,
				widget  = wibox.container.margin
			},
			nil,
			create_callback = function(self, c, index, objects) --luacheck: no unused args
				self:get_children_by_id('clienticon')[1].client = c
			end,
			layout = wibox.layout.align.vertical,
		},
	}

	s.mywibox = awful.wibar {
		width = dpi(2560),
		height = dpi(50),
		ontop = false,
		screen = s,
		expand = true,
		visible = true,
		bg = "alpha",
		border_width = dpi(0),
		border_color = theme.bg_light,
		position = "top",
	}

	s.mywibox:setup {
		layout = wibox.layout.align.horizontal,
		{ -- Left widgets
			{
				layout = wibox.layout.fixed.horizontal,
				{
					{
						layout = wibox.layout.align.horizontal,
						half_spr,
						tux,
					},
					widget = wibox.container.background,
				},
				half_spr,
				{
					layout = wibox.layout.align.horizontal,
					s.mytaglist,
				},
				{
						layout = wibox.layout.fixed.horizontal,
						half_spr,
            s.mypromptbox,
						half_spr,
				},
				half_spr,
			},
			widget = wibox.container.margin,
			top = dpi(5),
			bottom = dpi(5),
			right = dpi(5),
			left = dpi(5)
		},
		{ -- Center widgets
			layout = wibox.container.place,
			--s.mytasklist,
		},
		{ -- Right widgets
			{
				layout = wibox.layout.fixed.horizontal,
				spr,
				spr,
				volume,
        updates,
				battery,
				calendar,
				clock,
				s.mylayoutbox,
				sep,
				-- wibox.container.margin(
					wibox.widget.systray(),
					-- dpi(3), dpi(3), dpi(3), dpi(3)
				-- ),
			},
			widget = wibox.container.margin,
			top = dpi(5),
			bottom = dpi(5),
			right = dpi(5),
			left = dpi(5)
		},
	}

end
-- }}}

return theme
