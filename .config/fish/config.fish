set fish_greeting 
set doasroot "doas"

alias cwrd "$HOME/.local/bin/cwordle.sh"

alias la "ls -A"
alias gs "git status"
alias mv "mv -i"
alias rm "rm -i"
alias nv "nvim"

alias bare='/usr/bin/git --git-dir=bare --work-tree=.'
alias barest='/usr/bin/git --git-dir=bare --work-tree=. status'

alias cfg='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
alias cfst='/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME status'

alias cp "cp -i"
alias df "df -h"
alias config "nvim ~/.config/fish/config.fish"
alias a "open -a "
alias fullpwd "pwd"
alias gtop "~/gtop/bin/gtop"
alias exa "exa -alhSU --octal-permissions"

# ARCOLINUX ALIASES (from zshrc)
#pacman unlock
alias unlock "$doasroot rm /var/lib/pacman/db.lck"
alias rmpacmanlock "$doasroot rm /var/lib/pacman/db.lck"

#arcolinux logout unlock
alias rmlogoutlock "$doasroot rm /tmp/arcologout.lock"


#free
alias free "free -mt"

#continue download
alias wget "wget -c"


# Aliases for software managment
# pacman or pm
alias pacman 'pacman --color auto'
alias update '$doasroot pacman -Syyu'

# paru as aur helper - updates everything
alias pksyua "paru -Syu --noconfirm"
alias upall "paru -Syu --noconfirm"


#grub update
alias update-grub "$doasroot grub-mkconfig -o /boot/grub/grub.cfg"

#add new fonts
alias update-fc '$doasroot fc-cache -fv'

#copy/paste all content of /etc/skel over to home folder - backup of config created - beware
alias skel '[ -d ~/.config ] || mkdir ~/.config && cp -Rf ~/.config ~/.config-backup-(date +%Y.%m.%d-%H.%M.%S) && cp -rf /etc/skel/* ~'
#backup contents of /etc/skel to hidden backup folder in home/user
alias bupskel 'cp -Rf /etc/skel ~/.skel-backup-(date +%Y.%m.%d-%H.%M.%S)'

#copy bashrc-latest over on bashrc - cb= copy bashrc
#alias cb='$doasroot cp /etc/skel/.bashrc ~/.bashrc && source ~/.bashrc'
#copy /etc/skel/.zshrc over on ~/.zshrc - cb= copy zshrc
alias cz '$doasroot cp /etc/skel/.zshrc ~/.zshrc && exec zsh'

#switch between bash and zsh
alias tofish "$doasroot chsh $USER -s /bin/fish && echo 'Now log out.'"
alias tobash "$doasroot chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh "$doasroot chsh $USER -s /bin/zsh && echo 'Now log out.'"

#switch between lightdm and sddm
alias tolightdm "$doasroot pacman -S lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings --noconfirm --needed ; sudo systemctl enable lightdm.service -f ; echo 'Lightm is active - reboot now'"
alias tosddm "$doasroot pacman -S sddm --noconfirm --needed ; sudo systemctl enable sddm.service -f ; echo 'Sddm is active - reboot now'"
alias toly "$doasroot pacman -S ly --noconfirm --needed ; sudo systemctl enable ly.service -f ; echo 'Ly is active - reboot now'"
alias togdm "$doasroot pacman -S gdm --noconfirm --needed ; sudo systemctl enable gdm.service -f ; echo 'Gdm is active - reboot now'"
alias tolxdm "$doasroot pacman -S lxdm --noconfirm --needed ; sudo systemctl enable lxdm.service -f ; echo 'Lxdm is active - reboot now'"

# kill commands
# quickly kill conkies
alias kc 'killall conky'
# quickly kill polybar
alias kp 'killall polybar'

#hardware info --short
alias hw "hwinfo --short"

#skip integrity check
alias paruskip 'paru -S --mflags --skipinteg'
alias yayskip 'yay -S --mflags --skipinteg'
alias trizenskip 'trizen -S --skipinteg'

#check vulnerabilities microcode
alias microcode 'grep . /sys/devices/system/cpu/vulnerabilities/*'

#get fastest mirrors in your neighborhood
alias mirror "$doasroot reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord "$doasroot reflector --latest 30 --number 10 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors "$doasroot reflector --latest 30 --number 10 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora "$doasroot reflector --latest 30 --number 10 --sort age --save /etc/pacman.d/mirrorlist"
#our experimental - best option for the moment
alias mirrorx "$doasroot reflector --age 6 --latest 20  --fastest 20 --threads 5 --sort rate --protocol https --save /etc/pacman.d/mirrorlist"
alias mirrorxx "$doasroot reflector --age 6 --latest 20  --fastest 20 --threads 20 --sort rate --protocol https --save /etc/pacman.d/mirrorlist"
alias ram 'rate-mirrors --allow-root arch | $doasroot tee /etc/pacman.d/mirrorlist'

#mounting the folder Public for exchange between host and guest on virtualbox
alias vbm "$doasroot /usr/local/bin/arcolinux-vbox-share"

#enabling vmware services
alias start-vmware "$doasroot systemctl enable --now vmtoolsd.service"

#shopt
#shopt -s autocd # change to named directory
#shopt -s cdspell # autocorrects cd misspellings
#shopt -s cmdhist # save multi-line commands in history as single line
#shopt -s dotglob
#shopt -s histappend # do not overwrite history
#shopt -s expand_aliases # expand aliases

#youtube download
alias yta-aac "yt-dlp --extract-audio --audio-format aac "
alias yta-best "yt-dlp --extract-audio --audio-format best "
alias yta-flac "yt-dlp --extract-audio --audio-format flac "
alias yta-mp3 "yt-dlp --extract-audio --audio-format mp3 "
alias ytv-best "yt-dlp -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --merge-output-format mp4 "

#Recent Installed Packages
alias rip "expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -200 | nl"
alias riplong "expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -3000 | nl"

#iso and version used to install ArcoLinux
alias iso "cat /etc/dev-rel | awk -F '=' '/ISO/ {print $2}'"

#Cleanup orphaned packages
alias cleanup '$doasroot pacman -Rns (pacman -Qtdq)'

#search content with ripgrep
alias rg "rg --sort path"

#get the error messages from journalctl
alias jctl "journalctl -p 3 -xb"

#nano for important configuration files
#know what you do in these files
alias nlxdm "$doasroot $EDITOR /etc/lxdm/lxdm.conf"
alias nlightdm "$doasroot $EDITOR /etc/lightdm/lightdm.conf"
alias npacman "$doasroot $EDITOR /etc/pacman.conf"
alias ngrub "$doasroot $EDITOR /etc/default/grub"
alias nconfgrub "$doasroot $EDITOR /boot/grub/grub.cfg"
alias nmkinitcpio "$doasroot $EDITOR /etc/mkinitcpio.conf"
alias nmirrorlist "$doasroot $EDITOR /etc/pacman.d/mirrorlist"
alias narcomirrorlist '$doasroot nano /etc/pacman.d/arcolinux-mirrorlist'
alias nsddm "$doasroot $EDITOR /etc/sddm.conf"
alias nsddmk "$doasroot $EDITOR /etc/sddm.conf.d/kde_settings.conf"
alias nfstab "$doasroot $EDITOR /etc/fstab"
alias nnsswitch "$doasroot $EDITOR /etc/nsswitch.conf"
alias nsamba "$doasroot $EDITOR /etc/samba/smb.conf"
alias ngnupgconf "$doasroot nano /etc/pacman.d/gnupg/gpg.conf"
alias nhosts "$doasroot $EDITOR /etc/hosts"
alias nb "$EDITOR ~/.bashrc"
alias nz "$EDITOR ~/.zshrc"

#gpg
#verify signature for isos
alias gpg-check "gpg2 --keyserver-options auto-key-retrieve --verify"
alias fix-gpg-check "gpg2 --keyserver-options auto-key-retrieve --verify"
#receive the key of a developer
alias gpg-retrieve "gpg2 --keyserver-options auto-key-retrieve --receive-keys"
alias fix-gpg-retrieve "gpg2 --keyserver-options auto-key-retrieve --receive-keys"
alias fix-keyserver "[ -d ~/.gnupg ] || mkdir ~/.gnupg ; cp /etc/pacman.d/gnupg/gpg.conf ~/.gnupg/ ; echo 'done'"

#fixes
alias fix-permissions "$doasroot chown -R $USER:$USER ~/.config ~/.local"
alias keyfix "/usr/local/bin/arcolinux-fix-pacman-databases-and-keys"
alias key-fix "/usr/local/bin/arcolinux-fix-pacman-databases-and-keys"
alias keys-fix "/usr/local/bin/arcolinux-fix-pacman-databases-and-keys"
alias fixkey "/usr/local/bin/arcolinux-fix-pacman-databases-and-keys"
alias fixkeys "/usr/local/bin/arcolinux-fix-pacman-databases-and-keys"
alias fix-key "/usr/local/bin/arcolinux-fix-pacman-databases-and-keys"
alias fix-keys "/usr/local/bin/arcolinux-fix-pacman-databases-and-keys"
alias fix-sddm-config "/usr/local/bin/arcolinux-fix-sddm-config"
alias fix-pacman-conf "/usr/local/bin/arcolinux-fix-pacman-conf"
alias fix-pacman-keyserver "/usr/local/bin/arcolinux-fix-pacman-gpg-conf"

#maintenance
alias big "expac -H M '%m\t%n' | sort -h | nl"
alias downgrada "$doasroot downgrade --ala-url https://ant.seedhost.eu/arcolinux/"

#hblock (stop tracking with hblock)
#use unhblock to stop using hblock
alias unhblock "hblock -S none -D none"

#systeminfo
alias probe "$doasroot -E hw-probe -all -upload"
alias sysfailed "systemctl list-units --failed"

#shutdown or reboot
alias ssn "$doasroot shutdown now"
alias sr "$doasroot reboot"

#update betterlockscreen images
alias bls "betterlockscreen -u /usr/share/backgrounds/arcolinux/"

#give the list of all installed desktops - xsessions desktops
alias xd "ls /usr/share/xsessions"

# # ex = EXtractor for all kinds of archives
# # usage: ex <file>

#Leftwm aliases
alias lti "leftwm-theme install"
alias ltu "leftwm-theme uninstall"
alias lta "leftwm-theme apply"
alias ltupd "leftwm-theme update"
alias ltupg "leftwm-theme upgrade"

#arcolinux applications
alias att "arcolinux-tweak-tool"
alias adt "arcolinux-desktop-trasher"
alias abl "arcolinux-betterlockscreen"
alias agm "arcolinux-get-mirrors"
alias amr "arcolinux-mirrorlist-rank-info"
alias aom "arcolinux-osbeck-as-mirror"
alias ars "arcolinux-reflector-simple"
alias atm "arcolinux-tellme"
alias avs "arcolinux-vbox-share"
alias awa "arcolinux-welcome-app"

#remove
alias rmgitcache "rm -r ~/.cache/git"

#moving your personal files and folders from /personal to ~
alias personal 'cp -Rf /personal/* ~'

function mkcd -a dir
    mkdir "$dir"
    cd "$dir"
end

function fish_user_key_bindings
    fish_vi_key_bindings
end

set EDITOR "emacsclient -c -a emacs ''"
set VISUAL "emacsclient -c -a emacs ''"

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end

function __history_previous_command
  switch (commandline -t)
 case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

bind ! __history_previous_command
bind '$' __history_previous_command_arguments
set PATH "/opt/local/bin:/opt/local/sbin:$PATH"

set fish_color_autosuggestion '#7d7d7d'
set fish_color_command green
set fish_color_error '#ff6c6b'
set fish_color_param brcyan


alias df='df -h'

set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

/usr/bin/starship init fish | source


set -x PKG_CONFIG_PATH /opt/local/lib/pkgconfig

export MICRO_TRUECOLOR=1

set -e fish_user_paths
set -U fish_user_paths $HOME/.local/bin $HOME/Applications $fish_user_paths

# DOOM EMACS

#alias doomsync "~/.emacs.d/bin/doom sync"
#alias doomdoctor "~/.emacs.d/bin/doom doctor"
#alias doomconfig "emacsclient ~/.config/doom/config.org"
#alias doomupgrade "~/.emacs.d/bin/doom upgrade"
#alias doompurge "~/.emacs.d/bin/doom purge"

#zoxide init fish | source
#hexyl init fish | source

# TokyoNight Color Palette
set -l foreground c0caf5
set -l selection 33467C
set -l comment 565f89
set -l red f7768e
set -l orange ff9e64
set -l yellow e0af68
set -l green 9ece6a
set -l purple 9d7cd8
set -l cyan 7dcfff
set -l pink bb9af7

# Syntax Highlighting Colors
set -g fish_color_normal $foreground
set -g fish_color_command $cyan
set -g fish_color_keyword $pink
set -g fish_color_quote $yellow
set -g fish_color_redirection $foreground
set -g fish_color_end $orange
set -g fish_color_error $red
set -g fish_color_param $purple
set -g fish_color_comment $comment
set -g fish_color_selection --background=$selection
set -g fish_color_search_match --background=$selection
set -g fish_color_operator $green
set -g fish_color_escape $pink
set -g fish_color_autosuggestion $comment

# Completion Pager Colors
set -g fish_pager_color_progress $comment
set -g fish_pager_color_prefix $cyan
set -g fish_pager_color_completion $foreground
set -g fish_pager_color_description $comment
    


set NIX_LINK $HOME/.nix-profile/etc/profile.d/nix.sh
export NIX_PATH="nixpkgs=$HOME/.nix-defexpr/channels/nixos-21.05"
export PATH="$PATH:/Users/RaghavDixit/Library/Application Support/Coursier/bin"
set -gx PATH $PATH "$HOME/doom/bin"
set -gx PATH $PATH "$HOME/.nix-profile/bin/"
alias ed "ed -p ':' "
set -gx PATH $PATH "$HOME/dwm/"
set -gx PATH $PATH "$HOME/.cargo/bin/"
set -gx PATH $PATH "$HOME/.config/xmonad/bin"

#colorscript -r
fastfetch
