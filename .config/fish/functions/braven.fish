# Defined via `source`
function braven --wraps=open\ -a\ Brave\\\ Browser\\\ Nightly.app --description alias\ braven\ open\ -a\ Brave\\\ Browser\\\ Nightly.app
  open -a Brave\ Browser\ Nightly.app $argv; 
end
