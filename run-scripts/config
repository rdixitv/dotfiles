#!/usr/env bash

DMENU="dmenu -i -l 20 -p"
VISUAL="emacsclient -c -a emacs"
DMEDITOR="emacsclient -c -a emacs"
DMBROWSER="brave"
DMBROWSER2="qutebrowser"
DMTERM="alacritty -e"
logout_locker1="$HOME/.local/bin/lock"
logout_locker2="betterlockscreen -l"
setbg_dir="$HOME/wallpapers/pics $HOME/wallpapers/pics/arcolinux* $HOME/wallpapers/pics/TokyoNight"
maim_dir="${HOME}/ScreenShots"
maim_file_prefix="maim"
use_imv=0
note_dir="${HOME}/notes"
wiki_docs_path="/usr/share/doc/arch-wiki/html/en"

bookman_show_source=1

# For help
declare -A help_list
help_list[xmonad keybindings]="$HOME/.config/xmonad/bin/xmonad-keys"
help_list[qtile keybindings]="$HOME/.config/qtile/qtile-keys"
help_list[awesome keybindings]="awesome-client 'hotkeys_popup.show_help'"
help_list[sxhkd keybindings]="$HOME/.config/sxhkd/keys"
help_list[dmenu scripts]="$HOME/run-scripts/dm-hub"
help_list[launch editor]="$EDITOR"
help_list[launch visual editor]="$VISUAL"

# For config-edit
declare -A confedit_list
confedit_list[alacritty]="$HOME/.config/alacritty/alacritty.yml"
confedit_list[awesome]="$HOME/.config/awesome/rc.lua"
confedit_list[bash]="$HOME/.bashrc"
confedit_list[doom emacs config.el]="$HOME/.config/doom/config.el"
confedit_list[doom emacs config.org]="$HOME/.config/doom/config.org"
confedit_list[doom emacs init]="$HOME/.config/doom/init.el"
confedit_list[doom emacs packages]="$HOME/.config/doom/packages.el"
confedit_list[dunst]="$HOME/.config/dunst/dunstrc"
confedit_list[fish]="$HOME/.config/fish/config.fish"
confedit_list[neovim]="$HOME/.config/nvim/init.vim"
confedit_list[picom]="$HOME/.config/picom/picom.conf"
confedit_list[polybar]="$HOME/.config/polybar/config"
confedit_list[qtile]="$HOME/.config/qtile/config.py"
confedit_list[qutebrowser]="$HOME/.config/qutebrowser/config.py"
confedit_list[sxhkd]="$HOME/.config/sxhkd/sxhkdrc"
confedit_list[vifm]="$HOME/.config/vifm/vifmrc"
confedit_list[vim]="$HOME/.vim/vimrc"
confedit_list[xmobar]="$HOME/.config/xmobar/xmobar-tokyo-night.hs"
confedit_list[xmonad]="$HOME/.config/xmonad/src/Main.hs"
confedit_list[xinitrc]="$HOME/.xinitrc"
confedit_list[xresources]="$HOME/.Xresources"
confedit_list[zsh]="$HOME/.zshrc"

declare -A websearch
# Syntax:
# websearch[name]="https://www.example.com/search?q="

# Search Engines
websearch[bing]="https://www.bing.com/search?q="
websearch[brave]="https://search.brave.com/search?q="
websearch[duckduckgo]="https://duckduckgo.com/?q="
websearch[gemini search \(https\)]="https://portal.mozz.us/gemini/geminispace.info/search%3F"
websearch[google]="https://www.google.com/search?q="
websearch[qwant]="https://www.qwant.com/?q="
websearch[swisscows]="https://swisscows.com/web?query="
websearch[yandex]="https://yandex.com/search/?text="
# Information/News
websearch[bbcnews]="https://www.bbc.co.uk/search?q="
websearch[cnn]="https://www.cnn.com/search?q="
websearch[googlenews]="https://news.google.com/search?q="
websearch[wikipedia]="https://en.wikipedia.org/w/index.php?search="
websearch[wiktionary]="https://en.wiktionary.org/w/index.php?search="
# Social Media
websearch[reddit]="https://www.reddit.com/search/?q="
websearch[odysee]="https://odysee.com/$/search?q="
websearch[youtube]="https://www.youtube.com/results?search_query="
# Linux
websearch[archaur]="https://aur.archlinux.org/packages/?O=0&K="
websearch[archpkg]="https://archlinux.org/packages/?sort=&q="
websearch[archwiki]="https://wiki.archlinux.org/index.php?search="
websearch[debianpkg]="https://packages.debian.org/search?suite=default&section=all&arch=any&searchon=names&keywords="
# Development
websearch[github]="https://github.com/search?q="
websearch[gitlab]="https://gitlab.com/search?search="
websearch[sourceforge]="https://sourceforge.net/directory/?q="
websearch[stackoverflow]="https://stackoverflow.com/search?q="

# translate_service="libretranslate"
translate_service="lingva"
libretranslate_instance="https://libretranslate.de"
lingva_instance="https://lingva.ml"

declare -A currencies

declare -A weather_location
weather_location[New York]="New+York"
weather_location[San Jose]="San+Jose"
weather_location[Bangalore]="Bangalore"
weather_location[San Francisco]="San+Francisco"
weather_location[Helsinki]="Helsinki"
weather_location[Tampere]="Tampere"
weather_location[Amsterdam]="Amsterdam"
weather_location[Antwerp]="Antwerp"
weather_location[Sydney]="Sydney"
weather_location[Sao Paulo]="Sao+Paulo"
weather_location[Chicago]="Madrid"
weather_location[Moscow]="Moscow"
weather_location[Paris]="Paris"
weather_location[London]="London"
weather_location[Oslo]="Oslo"
weather_location[Stockholm]="Stockholm"
weather_location[Tampere]="Tampere"
